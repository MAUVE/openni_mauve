/*****
 * Copyright 2018 ONERA
 *
 * This file is part of the MAUVE OPENNI project.
 *
 * MAUVE OPENNI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE OPENNI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
*****/
#include "openni/mauve/OpenNI.hpp"
#include <opencv2/opencv.hpp>

namespace openni {
namespace mauve {

bool DeviceShell::configure_hook() {
  ::openni::OpenNI::initialize();
  return true;
}

void DeviceShell::cleanup_hook() {
  ::openni::OpenNI::shutdown();
}

bool DeviceCore::configure_hook() {

  std::string uri = shell().uri.get_value();

  ::openni::Status s;
  if (uri.empty()) s = device.open(::openni::ANY_DEVICE);
  else s = device.open(uri.c_str());

  if (s == ::openni::STATUS_OK) {
    auto info = device.getDeviceInfo();
    logger().info("Opened device {} on URI {}", info.getName(), info.getUri());

    colorStream.create(device, ::openni::SENSOR_COLOR);
    depthStream.create(device, ::openni::SENSOR_DEPTH);

    colorStream.setMirroringEnabled(false);
    depthStream.setMirroringEnabled(false);

    auto dmode = depthStream.getVideoMode();
    depthImage.image = new cv::Mat(dmode.getResolutionX(), dmode.getResolutionY(), CV_8U);
    auto cmode = colorStream.getVideoMode();
    colorImage.image = new cv::Mat(cmode.getResolutionX(), cmode.getResolutionY(), CV_8UC3);

    return ( colorStream.start() == ::openni::STATUS_OK )
      && ( depthStream.start() == ::openni::STATUS_OK );

  } else {
    logger().error("Failed to open device on URI {}", shell().uri.get_value());
    logger().info("Available devices:");
    ::openni::Array<::openni::DeviceInfo> devices;
    ::openni::OpenNI::enumerateDevices(&devices);
    for (int i = 0; i < devices.getSize(); i++) {
      ::openni::DeviceInfo d = devices[i];
      logger().info("{}/{} on URI {}", d.getName(), d.getVendor(), d.getUri());
    }
    return false;
  }
}

void DeviceCore::cleanup_hook() {
  if (colorStream.isValid()) colorStream.stop();
  if (depthStream.isValid()) depthStream.stop();
  device.close();
}

void DeviceCore::update() {
  if (colorStream.readFrame(&colorFrame) == ::openni::STATUS_OK) {
    cv::Mat mColor(colorFrame.getHeight(), colorFrame.getWidth(),
      CV_8UC3, const_cast<void*>( colorFrame.getData() ) );
    colorImage.image = &mColor;
    colorImage.width = colorFrame.getWidth();
    colorImage.height = colorFrame.getHeight();
    colorImage.encoding = ::mauve::types::sensor::image_encodings::RGB8;
    logger().debug("Grabbed color image of size {}x{}", colorImage.width, colorImage.height);
    shell().color.write(colorImage);
  }
  if (depthStream.readFrame(&depthFrame) == ::openni::STATUS_OK) {
    cv::Mat mDepth(depthFrame.getHeight(), depthFrame.getWidth(),
      CV_16UC1, const_cast<void*>( depthFrame.getData() ) );
    mDepth.convertTo(* depthImage.image, CV_8U, 255.0 / 10000);
    depthImage.width = depthFrame.getWidth();
    depthImage.height = depthFrame.getHeight();
    depthImage.encoding = ::mauve::types::sensor::image_encodings::MONO8;
    logger().debug("Grabbed depth image of size {}x{}", depthImage.width, depthImage.height);
    shell().depth.write(depthImage);
  }
}

  }
}
