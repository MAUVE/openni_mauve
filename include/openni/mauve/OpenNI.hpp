/*****
 * Copyright 2018 ONERA
 *
 * This file is part of the MAUVE OPENNI project.
 *
 * MAUVE OPENNI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE OPENNI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
*****/
#ifndef MAUVE_OPENNI_CAMERA_HPP
#define MAUVE_OPENNI_CAMERA_HPP

#include <mauve/runtime.hpp>
#include <mauve/base/PeriodicStateMachine.hpp>
#include <mauve/types/sensor/Image.hpp>
#include <openni2/OpenNI.h>

namespace openni {
  namespace mauve {

    /** OpenNI device shell */
    struct DeviceShell : public ::mauve::runtime::Shell {
      /** Output port for color image */
      ::mauve::runtime::WritePort<::mauve::types::sensor::Image>& color
        = mk_write_port<::mauve::types::sensor::Image>("color");
      /** Output port for depth image */
      ::mauve::runtime::WritePort<::mauve::types::sensor::Image>& depth
        = mk_write_port<::mauve::types::sensor::Image>("depth");
      /** Device ID Property */
      ::mauve::runtime::Property<std::string>& uri
        = mk_property<std::string>("uri", "");
    protected:
      virtual bool configure_hook() override;
      void cleanup_hook() override;
    };

    /** OpenNI device core */
    struct DeviceCore : public ::mauve::runtime::Core<DeviceShell> {
      /** Grab and publish color and depth images */
      void update();
    protected:
      virtual bool configure_hook() override;
      void cleanup_hook() override;
    private:
      ::openni::Device device;
      ::openni::VideoStream colorStream, depthStream;
      ::openni::VideoFrameRef colorFrame, depthFrame;
      ::mauve::types::sensor::Image depthImage, colorImage;
    };

    using Device = ::mauve::runtime::Component<DeviceShell, DeviceCore,
      ::mauve::base::PeriodicStateMachine<DeviceShell, DeviceCore> >;

  }
}

#endif // MAUVE_V4L_CAMERA_HPP
